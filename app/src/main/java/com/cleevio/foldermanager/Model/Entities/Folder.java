package com.cleevio.foldermanager.Model.Entities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by filas on 21.10.2016.
 */

public class Folder {

    private String name;
    private Drawable image;
    private String path;

    public Folder(String name, Drawable image , String path) {
        this.image = image;
        this.name = name;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public Drawable getImage() {
        return image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Folder folder = (Folder) o;

        if (name != null ? !name.equals(folder.name) : folder.name != null) return false;
        return path != null ? path.equals(folder.path) : folder.path == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}
