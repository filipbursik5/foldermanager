package com.cleevio.foldermanager.Model.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cleevio.foldermanager.Model.Entities.Folder;
import com.cleevio.foldermanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by filas on 21.10.2016.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private List<Folder> folderList;
    private List<Integer> deleteFolders;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(Folder item, View view, int position);
    }

    public interface OnLongItemClickListener {
        void onItemLongClick(Folder item);
    }

    private final OnItemClickListener listener;
    private final OnLongItemClickListener longListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            image = (ImageView) view.findViewById(R.id.image);
        }
    }

    public ListAdapter(List<Folder> folderList, OnItemClickListener listener, OnLongItemClickListener longListener, Context context, List<Integer> deleteFolders) {
        this.listener = listener;
        this.context = context;
        this.folderList = folderList;
        this.longListener = longListener;
        this.deleteFolders = deleteFolders;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Folder folder = folderList.get(position);
        holder.title.setText(folder.getName());
        holder.image.setBackground(folder.getImage());

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                longListener.onItemLongClick(folder);
                return false;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(folder, view, folderList.indexOf(folder));
            }
        });

        if (deleteFolders.contains(position)) {
            holder.itemView.setBackgroundColor(Color.GRAY);
        } else {
            if (position % 2 == 0) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
            } else {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        }
    }

    @Override
    public int getItemCount() {
        return folderList.size();
    }

}
