package com.cleevio.foldermanager.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cleevio.foldermanager.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by filas on 21.10.2016.
 */

public class SettingsFragment extends PreferenceFragment {

    private EditTextPreference defaultFolder;
    private boolean ended;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

        defaultFolder = (EditTextPreference) findPreference("edittext_preference");
        defaultFolder.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newVal) {
                return true;
            }
        });

        defaultFolder.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                ended = false;
                findFolder(Environment.getExternalStorageDirectory(), newValue.toString());
                if (!ended) {
                    Toast.makeText(getContext(), "File not found!", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(android.R.color.white));

        return view;
    }

    private void findFolder(File root, String file) {

        if (file.equalsIgnoreCase("root")) {
            writeToFile(Environment.getExternalStorageDirectory().getPath());
            ended = true;
        } else {

            File[] files = root.listFiles();
            for (File singleFile : files) {
                if (singleFile.isDirectory()) {
                    if (singleFile.getName().equals(file) && !ended) {
                        writeToFile(singleFile.getPath());
                        return;
                    } else {
                        findFolder(singleFile, file);
                    }
                }
            }
        }
    }

    private void writeToFile(String path) {
        BufferedWriter writer = null;
        try {
            File file = new File(getContext().getFilesDir(), "def.fm");

            writer = new BufferedWriter(new FileWriter(file));

            if (!file.exists()) {
                file.delete();
                file.createNewFile();
            }

            writer.write(path);
            writer.close();
        } catch (IOException e) {
            Log.d("Writing to file", e.toString());
        }
        ended = true;
    }
}
