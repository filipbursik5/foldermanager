package com.cleevio.foldermanager.Activities;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.cleevio.foldermanager.Model.Adapters.GridAdapter;
import com.cleevio.foldermanager.Model.Adapters.ListAdapter;
import com.cleevio.foldermanager.Model.Entities.Folder;
import com.cleevio.foldermanager.Fragments.SettingsFragment;
import com.cleevio.foldermanager.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

public class FolderActivity extends AppCompatActivity {

    //=== Views ===
    private RecyclerView recyclerView;
    private GridView gridView;
    private TextView noFolder;
    private TextView pathFolder;

    //=== Adapters ===
    private ListAdapter mAdapter;
    private GridAdapter mAdapterGrid;

    private Fragment frg = new SettingsFragment();
    private FindFolders findFiles = new FindFolders();
    private ActionMode actionMode = null;

    private static List<Folder> fl = new ArrayList<>();
    private static List<Integer> deleteFolders = new ArrayList<>();

    private static Stack<File> stack = new Stack<>();

    private String defaultFile = Environment.getExternalStorageDirectory().getPath();
    private boolean exit = false;
    private static boolean action = false;

    //=== Implement ActionMode callbacks ===
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            action = true;
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_folder_action, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            for (int i = 0; i < deleteFolders.size(); i++) {
                Toast.makeText(getApplicationContext(), fl.get(deleteFolders.get(i)).getName(), Toast.LENGTH_SHORT).show();
                new File(fl.get(deleteFolders.get(i)).getPath()).delete();
            }

            deleteFolders.clear();
            findFiles(stack.peek().getPath());
            actionMode.finish();
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            action = false;
            deleteFolders.clear();
            findFiles(stack.peek().getPath());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);*/

        //=== Find view by ID ===
        noFolder = (TextView) findViewById(R.id.no_folder);
        pathFolder = (TextView) findViewById(R.id.path);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        gridView = (GridView) findViewById(R.id.grid_view);

        //=== First initialization (read default folder from file) and set first item for stack trace ===
        init();

        //=== Setup adapters for grid and recycler view and listeners for them ===
        setupAdapter();

        //=== Set adapters to GridView or Recycle view (depends on device orientation)
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else {
            gridView.setAdapter(mAdapterGrid);
        }

        //=== Find files in default folder, by thread
        findFiles.start();

        if (action) {
            actionMode = startSupportActionMode(mActionModeCallback);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_folder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case (R.id.action_settings):
                getFragmentManager().beginTransaction()
                        .replace(android.R.id.content, frg).commit();
                break;

            case (R.id.action_refresh):
                findFiles(stack.peek().getPath());
                Toast.makeText(this, "Refreshed!", Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //=== Setup adapters for all views ===
    private void setupAdapter() {
        mAdapter = new ListAdapter(fl, new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Folder item, View view, int position) {
                if (!action) {
                    if (new File(item.getPath()).isDirectory()) {
                        stack.add(new File(item.getPath()));
                        findFiles(stack.peek().getPath());
                    } else {
                        Uri uri = Uri.fromFile(new File(item.getPath()));
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                        String mime = "*/*";
                        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                        if (mimeTypeMap.hasExtension(
                                mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
                            mime = mimeTypeMap.getMimeTypeFromExtension(
                                    mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
                        intent.setDataAndType(uri, mime);
                        startActivity(intent);
                    }
                } else {
                    if (!deleteFolders.contains(fl.indexOf(item))) {
                        deleteFolders.add(fl.indexOf(item));
                        view.setBackgroundColor(Color.GRAY);
                    } else {
                        if (position % 2 == 0) {
                            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));
                        } else {
                            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                        }
                        deleteFolders.remove(deleteFolders.indexOf(fl.indexOf(item)));
                    }
                }

            }
        }, new ListAdapter.OnLongItemClickListener() {
            @Override
            public void onItemLongClick(Folder item) {
                actionMode = startSupportActionMode(mActionModeCallback);
            }
        }, getApplicationContext(), deleteFolders);

        mAdapterGrid = new GridAdapter(fl, new GridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Folder item, View v, int position) {
                if (!action) {
                    if (new File(item.getPath()).isDirectory()) {
                        stack.add(new File(item.getPath()));
                        findFiles(stack.peek().getPath());
                    } else {
                        Uri uri = Uri.fromFile(new File(item.getPath()));
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                        String mime = "*/*";
                        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                        if (mimeTypeMap.hasExtension(
                                mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
                            mime = mimeTypeMap.getMimeTypeFromExtension(
                                    mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
                        intent.setDataAndType(uri, mime);
                        startActivity(intent);
                    }
                } else {
                    if (!deleteFolders.contains(fl.indexOf(item))) {
                        deleteFolders.add(fl.indexOf(item));
                        v.setBackgroundColor(Color.GRAY);
                    } else {
                        v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));
                        deleteFolders.remove(deleteFolders.indexOf(fl.indexOf(item)));
                    }
                }
            }
        }, new GridAdapter.OnLongItemClickListener() {
            @Override
            public void onItemLongClick(Folder item) {
                actionMode = startSupportActionMode(mActionModeCallback);
            }
        }, getApplicationContext(), deleteFolders);
    }

    //=== First initialization or after change settings ===
    private void init() {
        if (stack.empty()) {
            BufferedReader br = null;
            try {
                File file = new File(getApplicationContext().getFilesDir(), "def.fm");
                br = new BufferedReader(new FileReader(file));
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();
                sb.append(line);
                defaultFile = sb.toString();
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stack.add(new File(defaultFile));
        }
    }

    //=== Method for check all files in path given in argument ===
    public void findFiles(String file) {
        File[] files = new File(file).listFiles();
        fl.clear();
        if (files != null) {

            //=== Print stack trace.. Depth of plunge from default folder ===
            String tmp = stack.peek().toString();
            tmp.replace("[", "").replace("]", "");
            tmp.replace("storage", "root");
            pathFolder.setText(tmp);

            if (files.length == 0) {
                noFolder.setText("No foldres!");
            } else {
                noFolder.setText("");
            }

            //=== Find all files in path given in argument ===
            for (File singleFile : files) {
                fl.add(new Folder(singleFile.getName(), singleFile.isDirectory() ? getResources().getDrawable(R.mipmap.folder) : getResources().getDrawable(R.mipmap.file), singleFile.getPath()));
            }

            //=== Sort the array of folders ===
            Collections.sort(fl, new Comparator<Folder>() {
                @Override
                public int compare(Folder folder, Folder t1) {
                    return folder.toString().compareToIgnoreCase(t1.toString());
                }
            });

            //=== Put non-folders files to the end ===
            for (int i = 0; i < fl.size(); i++) {
                if (!new File(fl.get(i).getPath()).isDirectory()) {
                    Folder tmp1 = fl.get(i);
                    fl.remove(tmp1);
                    fl.add(tmp1);
                }
            }

            //=== Refresh adapter ===
            if (recyclerView != null && !recyclerView.isComputingLayout()) {
                mAdapter.notifyDataSetChanged();
            } else if (gridView != null) {
                mAdapterGrid.notifyDataSetChanged();
            }
        }
    }

    //=== Overrided method, after double click and if stack is empty then finish the app..
    //If stack is not empty, then going back to default folder ===
    @Override
    public void onBackPressed() {
        if (frg.isAdded()) {
            getFragmentManager().beginTransaction()
                    .remove(frg).commit();
            stack.clear();
            init();
            findFiles(stack.peek().toString());
        } else {
            if (exit) {
                finish();
            } else {
                if (stack.size() > 1) {
                    stack.pop();
                    findFiles(stack.peek().getPath());
                } else {
                    Toast.makeText(this, "Click on BACK again to exit the APP", Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 2000);
                }
            }
        }
    }

    //=== Thread for first folders finding ===
    public class FindFolders extends Thread {

        @Override
        public void run() {
            findFiles(stack.peek().getPath());
        }

    }
}
